/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_tab.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lobertin <lobertin@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 16:46:29 by lobertin          #+#    #+#             */
/*   Updated: 2019/11/19 16:47:17 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int		check(t_map map)
{
	map.l = 0;
	map.y = 1;
	map.x = 0;
	if (!(map.map[map.y][map.x]))
		return (0);
	while (map.map[1][map.l])
		map.l++;
	while (map.map[map.y])
	{
		map.x = 0;
		while (map.map[map.y][map.x])
		{
			if (map.map[map.y][map.x] != map.symbols[EMPT] &&
				map.map[map.y][map.x] != map.symbols[OBST] &&
				map.map[map.y][map.x] != map.symbols[FULL])
				return (0);
			map.x++;
		}
		if (map.x != map.l)
			return (0);
		map.y++;
	}
	if (map.y - 1 != map.height)
		return (0);
	return (1);
}

int		check_map(t_map map)
{
	if (map.symbols[EMPT] == map.symbols[OBST] ||
		map.symbols[EMPT] == map.symbols[FULL] ||
		map.symbols[OBST] == map.symbols[FULL])
		return (0);
	if (map.symbols[EMPT] <= ' ' || map.symbols[EMPT] >= '~' ||
		map.symbols[OBST] <= ' ' || map.symbols[OBST] >= '~' ||
		map.symbols[FULL] <= ' ' || map.symbols[FULL] >= '~')
		return (0);
	if (!(map.symbols[EMPT] && map.symbols[OBST] && map.symbols[FULL]))
		return (0);
	return (check(map));
}
