/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_tab.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 17:27:37 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/20 15:27:08 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int		get_fstat(t_fstat *fstat, char *filename, int stdin_filen)
{
	char	buffer[BUF_SIZE];
	int		bytes_read;

	if (stdin_filen == STDIN)
		fstat->fd = 0;
	else if ((fstat->fd = open(filename, O_RDONLY)) == -1)
		return (0);
	fstat->ch = 0;
	while ((bytes_read = read(fstat->fd, buffer, BUF_SIZE)) > 0)
		fstat->ch += bytes_read;
	if (close(fstat->fd) == -1)
		return (0);
	return (1);
}

int		get_file(t_fstat fstat, char **out, char *filename, int stdin_filen)
{
	if (stdin_filen == STDIN)
		fstat.fd = 0;
	else if ((fstat.fd = open(filename, O_RDONLY)) == -1)
		return (0);
	if (read(fstat.fd, *out, fstat.ch))
	{
		if (close(fstat.fd) == -1)
			return (0);
		return (1);
	}
	return (0);
}

int		get_tab(t_map *map, char *filename, int stdin_filen)
{
	char	*to_split;
	t_fstat fstat;

	fstat = (t_fstat){0, 0, 0, 0};
	if (get_fstat(&fstat, filename, stdin_filen) == 0)
		return (0);
	to_split = malloc(sizeof(*to_split) * (fstat.ch + 1));
	if ((get_file(fstat, &to_split, filename, stdin_filen)) == 0)
		return (0);
	map->map = split_to_tab(to_split);
	if (get_params(to_split, map) == 0)
		return (0);
	free(to_split);
	return (1);
}

int		get_params(char *to_split, t_map *map)
{
	int	i;
	int	len;

	len = 0;
	while (to_split[len] != '\n')
		len++;
	i = 0;
	while (i < len - 3)
	{
		if (!(to_split[i] >= '0' && to_split[i] <= '9'))
			return (0);
		map->height = (map->height * 10) + (to_split[i] - '0');
		i++;
	}
	map->width = map->height;
	i = 0;
	while (i < len)
	{
		map->symbols[i] = to_split[i + (len - 3)];
		i++;
	}
	return (1);
}
