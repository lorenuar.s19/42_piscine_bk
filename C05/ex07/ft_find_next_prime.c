/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 11:08:54 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/18 14:29:03 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_prime(int nb)
{
	int i;

	i = 1;
	if (nb < 2)
		return (0);
	else if (nb == 2)
		return (1);
	else if (nb > 2)
	{
		while (i < (nb / i))
		{
			if ((nb % ++i) == 0)
				return (0);
		}
	}
	return (1);
}

int		ft_find_next_prime(int nb)
{
	if (nb > 2)
	{
		while (!ft_is_prime(nb))
			nb++;
		return (nb);
	}
	return (2);
}
