/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 14:45:10 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/13 16:49:09 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	abs(int n)
{
	return ((n < 0) ? -n : n);
}

int	ft_ultimate_range(int **range, int min, int max)
{
	int	size;
	int i;

	if (min >= max)
	{
		*range = 0;
		return (0);
	}
	size = abs(min - max);
	*range = malloc(sizeof(int) * size - 1);
	if (*range)
	{
		i = 0;
		while (min < max)
			(*range)[i++] = min++;
		return (size);
	}
	return (-1);
}
