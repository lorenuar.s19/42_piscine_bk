/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/12 11:11:43 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/13 16:48:15 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len])
		len++;
	return (len);
}

char	*ft_strcpy(char *dest, char *src)
{
	int i;

	if (src)
	{
		i = 0;
		while (i < ft_strlen(src))
		{
			dest[i] = src[i];
			i++;
		}
		dest[i] = '\0';
		return (dest);
	}
	return (0);
}

char	*ft_strdup(char *src)
{
	char *cpy;

	if (src)
	{
		cpy = malloc(sizeof(cpy) * ft_strlen(src) + 1);
		ft_strcpy(cpy, src);
		return (cpy);
	}
	return (0);
}
