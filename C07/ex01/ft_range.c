/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 14:19:10 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/13 16:49:47 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	abs(int n)
{
	if (n < 0)
		return (-n);
	return (n);
}

int	*ft_range(int min, int max)
{
	int	*tab;
	int	range;
	int	i;

	if (min > max)
		return (0);
	tab = 0;
	range = abs(min - max);
	tab = malloc(sizeof(int) * range);
	if (tab)
	{
		i = 0;
		while (min < range)
		{
			tab[i] = min++;
			i++;
		}
		return (tab);
	}
	return (0);
}
