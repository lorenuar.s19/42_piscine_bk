/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 14:02:27 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/21 11:17:34 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	int len;

	len = 0;
	while (str[len])
		len++;
	return (len);
}

char	*ft_strcat(char *dest, char *src)
{
	int	i;
	int	j;

	i = 0;
	while (dest[i])
		i++;
	j = 0;
	while (src[j])
		dest[i++] = src[j++];
	dest[i] = '\0';
	return (dest);
}

void	concat(char **strs, char **joined, char *sep, int size)
{
	int	i;

	i = 0;
	while (i < size)
	{
		ft_strcat(*joined, strs[i]);
		i++;
		if (i < size)
			ft_strcat(*joined, sep);
	}
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	char	*joined;
	int		i;
	int		total_len;

	joined = 0;
	if (size == 0)
		return (joined);
	else
	{
		total_len = 0;
		i = 0;
		while (i < size)
			total_len += ft_strlen(strs[i++]);
		total_len += ft_strlen(sep) * (size - 1);
		if ((joined = malloc((sizeof(char) * total_len) + 1)) != 0)
		{
			concat(strs, &joined, sep, size);
			return (joined);
		}
	}
	return ("\0");
}
