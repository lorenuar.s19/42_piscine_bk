/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/31 09:11:20 by lorenuar          #+#    #+#             */
/*   Updated: 2019/10/31 15:01:23 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

#define INT_MIN -2147483648
#define INT_MAX 2147483647
#define BASE 10

void	pc(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb)
{
	long i;

	i = nb;
	if (i < 0)
		pc('-');
	if (nb >= INT_MAX)
		write(1, "2147483647", 10);
	else if (i <= INT_MIN)
		write(1, "2147483648", 10);
	else if (i > INT_MIN && i < INT_MAX)
	{
		if (i < 0)
			i = -i;
		if (i >= BASE)
		{
			ft_putnbr(i / 10);
			ft_putnbr(i % 10);
		}
		else
			pc(i + '0');
	}
}
