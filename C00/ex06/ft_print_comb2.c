/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/30 17:00:05 by lorenuar          #+#    #+#             */
/*   Updated: 2019/10/31 13:07:35 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	print(int a, int b)
{
	int	aa;
	int	aaa;
	int	bb;
	int	bbb;

	aa = a / 10 + '0';
	aaa = a % 10 + '0';
	bb = b / 10 + '0';
	bbb = b % 10 + '0';
	write(1, &aa	, 1);
	write(1, &aaa	, 1);
	write(1, " "	, 1);
	write(1, &bb	, 1);
	write(1, &bbb	, 1);
	if (a != 98)
		write(1, ", ", 2);
}

void	ft_print_comb2(void)
{
	int a;
	int b;

	a = 0;
	while (a <= 98)
	{
		b = 0;
		while (b <= 99)
		{
			if (a < b)
				print(a, b);
			b++;
		}
		a++;
	}
}
