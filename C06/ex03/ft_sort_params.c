/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 11:22:56 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/18 11:59:10 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_strcmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i] == s2[i])
		i++;
	return (s1[i] - s2[i]);
}

void	ft_sort_str_tab(char ***tab, int size)
{
	int		i;
	int		sorted;
	int		sort_count;
	char	*temp;

	sorted = 0;
	while (!sorted)
	{
		i = 1;
		sort_count = 0;
		while (i < size - 1)
		{
			if (ft_strcmp((*tab)[i], (*tab)[i + 1]) > 0)
			{
				temp = (*tab)[i];
				(*tab)[i] = (*tab)[i + 1];
				(*tab)[i + 1] = temp;
				sort_count++;
			}
			i++;
		}
		if (!sort_count)
			sorted = 1;
	}
}

int		ft_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len])
		len++;
	return (len);
}

int		main(int argc, char **argv)
{
	int i;

	if (argc > 1)
	{
		ft_sort_str_tab(&argv, argc);
		i = 1;
		while (i < argc)
		{
			write(1, argv[i], ft_strlen(argv[i]));
			write(1, "\n", 1);
			i++;
		}
	}
	return (0);
}
