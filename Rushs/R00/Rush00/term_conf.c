/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   testColors.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/02 16:56:42 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/02 18:32:33 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr(int nb);

void ps(char *s)
{
	while (*s)
		write(1, s++, 1);
}
void pc(char c)
{
	write(1, &c, 1);
}

void rst_term()
{
	ps("\x1b[0m");
}

void term_conf(int f_b, int r,int g, int b)
{
	pc('\x1b');
	pc('[');
	if(f_b)
		ft_putnbr(38);
	else if (!f_b)
		ft_putnbr(48);
	ps(";2;");
	ft_putnbr(r);
	pc(';');
	ft_putnbr(g);
	pc(';');
	ft_putnbr(b);
	pc('m');
}
