/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   testColors.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/02 16:56:42 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/02 18:24:13 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr(int nb);

void ps(char *s)
{
	while (*s)
		write(1, s++, 1);
}
void pc(char c)
{
	write(1, &c, 1);
}

void rst_term()
{
	ps("\x1b[0m");
}

void conf_term(int f_b, int r,int g, int b)
{
	pc('\x1b');
	pc('[');
	if(f_b)
		ft_putnbr(38);
	else if (!f_b)
		ft_putnbr(48);
	ps(";2;");
	ft_putnbr(r);
	pc(';');
	ft_putnbr(g);
	pc(';');
	ft_putnbr(b);
	pc('m');
}

int main(void)
{
	double ar = 50.0;
	double ag = 50.0;
	double ab = 50.0;
	double ara = 0.005;
	double aga = 0.01;
	double aba = 0.002;
	
	int fr = 0;
	int fg = 0;
	int fb = 0;
	int br = 0;
	int bg = 0;
	int bb = 0;

	while (1)
	{
		/*printf("\n ar : %D | ag : %D | ab : %D", ar, ag, ab);*/
		conf_term(0, (int)ar, (int)ag, (int)ab);
		ps("\n");
		ps("\x1b[0m");
		ar += ara;
		ag += aga;
		ab += aba;

		if (ar > 250)
			ara = -ara;
		if (ar < 25)
			ara = -ara;
		
		if (ag > 200)
			aga = -aga;
		if (ag < 25)
			aga = -aga;
		
		if (ab > 250)
			aba = -aba;
		if (ab < 25)
			aba = -aba;
		
		usleep(10);
	}
	rst_term();
	return(0);
}
