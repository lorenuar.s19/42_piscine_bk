/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush02.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/02 10:14:05 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/02 11:51:27 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	top(int y)
{
	int yy;

	yy = y;
	ft_putchar('A');
	while (y - 2 > 0)
	{
		ft_putchar('B');
		y--;
	}
	if (yy > 1)
		ft_putchar('A');
	ft_putchar('\n');
}

void	body(int x, int y)
{
	int yy;

	yy = y;
	while (x)
	{
		ft_putchar('B');
		y = yy;
		while (y - 2 > 0)
		{
			ft_putchar(' ');
			y--;
		}
		x--;
		if (yy > 1)
			ft_putchar('B');
		ft_putchar('\n');
	}

}

void	bot(int y)
{
	int yy;

	yy = y;
	ft_putchar('C');
	while (y - 2 > 0)
	{
		ft_putchar('B');
		y--;
	}
	if (yy > 1)
		ft_putchar('C');
	ft_putchar('\n');
}

void	rush(int x, int y)
{
		if (x && y)
		{
			top(y);
			if (x >= 3)
				body(x - 2, y);
			if (x >=  2)
				bot(y);
		}

}
