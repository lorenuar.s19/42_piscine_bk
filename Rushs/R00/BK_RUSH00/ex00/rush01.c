/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush01.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/02 14:45:51 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/02 15:23:11 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	print(char *str, int x, int y)
{
	int xx;

	xx = x;
	while (y > 0)
	{
		ft_putchar(str[0]);
		x = xx;
		while (x - 2 > 0)
		{
			ft_putchar(str[1]);
			x--;
		}
		y--;
		if (xx > 1)
			ft_putchar(str[2]);
		ft_putchar('\n');
	}
}

void	rush(int x, int y)
{
	if (x > 0 && y > 0)
	{
		print("/*\\", x, 1);
		if (y >= 3)
			print("* *", x, y - 2);
		if (y >=  2)
			print("\\*/", x, 1);
	}
}
