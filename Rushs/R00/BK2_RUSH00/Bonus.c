/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bonus.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/03 15:04:31 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/03 16:23:45 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

typedef struct s_S3tring
{
	char top[3];
	char bod[3];
	char bot[3];
} t_str3 ;

void ft_puts(char *s)
{
	while (*s)
		write(1, s++, 1);
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}
int			ft_atoi(char * str)
{
	int		i;
	int		n;
	int		sign;

	i = 0;
	n = 0;
	sign = 1;
	if (str[0] == '-')
	{
		sign--;
		i++;
	}
	else if (str[0] == '+')
		i++;
	while (str[i] >= '0' && str[i] <= '9')
	{
		n *= 10;
		n += (int)str[i] - '0';
		i++;
	}
	if (!sign)
		n *= -1;
	return (n);
}

void	print(char *str, int x, int y)
{
	int xx;

	xx = x;
	while (y > 0)
	{
		ft_putchar(str[0]);
		x = xx;
		while (x - 2 > 0)
		{
			ft_putchar(str[1]);
			x--;
		}
		y--;
		if (xx > 1)
		{
			ft_putchar(str[2]);
		}
		ft_putchar('\n');
	}
}

void	rush(int x, int y, t_str3 str3)
{
	if (x > 0 && y > 0)
	{
		print(str3.top, x, 1);
		if (y >= 3)
			print(str3.bod, x, y - 2);
		if (y >= 2)
			print(str3.bot, x, 1);
	}
}
char	*ft_strncpy(char *src, char *dest, int n)
{
	char *s;

	s = dest;
	while(*src && n > 0)
		*dest++ = *src++;
	return (s);
}

int		main(int argc, char *argv[])
{
	int x;
	int y;
	t_str3 str3;

	str3 = (t_str3){"   ", "   ","   " };
	if (argc == 6)
	{
		x = ft_atoi(argv[1]);
		y = ft_atoi(argv[2]);
		ft_strncpy(argv[3], str3.top, 3);
		ft_strncpy(argv[4], str3.bod, 3);
		ft_strncpy(argv[5], str3.bot, 3);
		rush(x, y, str3);
	}
	else if(argc == 3)
	{
		x = ft_atoi(argv[1]);
		y = ft_atoi(argv[2]);
		str3 = (t_str3){"   ", "   ","   " };

		rush(x, y, str3);
	}

	else
		ft_puts("Usage : \n bonus [taille x] [taille y] "
				"'3 * (top chars)' '3 * (body chars)' '3 * (bottom chars)'\n");

	return (0);
}
