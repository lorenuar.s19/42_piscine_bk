/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   numbers.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/16 18:48:09 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/17 10:19:06 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "_h/rush02.h"

char	*units(t_dict dict, char c)
{
	if (c == '0')
		return (dict.names[0]);
	else if (c == '1')
		return (dict.names[1]);
	else if (c == '2')
		return (dict.names[2]);
	else if (c == '3')
		return (dict.names[3]);
	else if (c == '4')
		return (dict.names[4]);
	else if (c == '5')
		return (dict.names[5]);
	else if (c == '6')
		return (dict.names[6]);
	else if (c == '7')
		return (dict.names[7]);
	else if (c == '8')
		return (dict.names[8]);
	else if (c == '9')
		return (dict.names[9]);

	return ("Error\n");
}

char	*tens(t_dict dict, char c)
{
	if (c == '1')
		return (dict.names[10]);
	else if (c == '2')
		return (dict.names[20]);
	else if (c == '3')
		return (dict.names[21]);
	else if (c == '4')
		return (dict.names[22]);
	else if (c == '5')
		return (dict.names[23]);
	else if (c == '6')
		return (dict.names[24]);
	else if (c == '7')
		return (dict.names[25]);
	else if (c == '8')
		return (dict.names[26]);
	else if (c == '9')
		return (dict.names[27]);
	return ("Error\n");
}

char	*special(t_dict dict, char c)
{
	if (c == '1')
		return (dict.names[11]);
	else if (c == '2')
		return (dict.names[12]);
	else if (c == '3')
		return (dict.names[13]);
	else if (c == '4')
		return (dict.names[14]);
	else if (c == '5')
		return (dict.names[15]);
	else if (c == '6')
		return (dict.names[16]);
	else if (c == '7')
		return (dict.names[17]);
	else if (c == '8')
		return (dict.names[18]);
	else if (c == '9')
		return (dict.names[19]);
	return ("Error\n");
}
/*
char	*big_numbers(t_dict dict, char c, int len)
{
	if (c == '100')
		return (dict.names[28]);
	else if (c == '1000')
		return (dict.names[29]);
	else if (c == '1000000')
		return (dict.names[30]);
	else if (c == '1000000000')
		return (dict.names[31]);
	else if (c == '1000000000000')
		return (dict.names[32]);
	else if (c == '1000000000000000')
		return (dict.names[33]);
	else if (c == '1000000000000000000')
		return (dict.names[34]);
	return ("Error\n");
}

char *bigbig_numbers(t_dict dict, char c, int len)
{
	if (c == '1000000000000000000000')
		return (dict.names[35]);
	else if (c == '1000000000000000000000000')
		return (dict.names[36]);
	else if (c == '1000000000000000000000000000')
		return (dict.names[37]);
	else if (c == '1000000000000000000000000000000')
		return (dict.names[38]);
	else if (c == '1000000000000000000000000000000000')
		return (dict.names[39]);
	else if (c =='1000000000000000000000000000000000000')
		return (dict.names[40]);
	return ("Error\n");
}*/

char	*big_nums(t_dict dict, int len)
{
	if (len == 3)
		return (dict.names[28]);
	else if (len == 4)
		return (dict.names[29]);
	else if (len == 7)
		return (dict.names[30]);
	else if (len == 10)
		return (dict.names[31]);
	else if (len == 13)
		return (dict.names[32]);
	else if (len == 16)
		return (dict.names[33]);
	else if (len == 19)
		return (dict.names[34]);
	else if (len == 22)
		return (dict.names[35]);
	else if (len == 25)
		return (dict.names[36]);
	else if (len == 28)
		return (dict.names[37]);
	else if (len == 31)
		return (dict.names[38]);
	else if (len == 34)
		return (dict.names[39]);
	else if (len == 37)
		return (dict.names[40]);
}
