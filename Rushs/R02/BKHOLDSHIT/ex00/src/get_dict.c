/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_dict.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/17 11:08:26 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/17 12:23:53 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dict.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/16 15:46:48 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/17 11:07:43 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#define de printf("%d\n",__LINE__);

#include "_h/rush02.h"

t_fstat		get_fstat_dict(char *filename)
{
	t_fstat fstat;
	char	buf[1];
	int		fd;
	int		len;

	fd = open(filename, O_RDONLY);
	fstat = (t_fstat){0,0,0};
	if (fd > 0)
	{
		while (read(fd, buf, 1) > 0)
		{
			if (buf[0] == '\n')
			{
				fstat.lines++;
				len = 0;
			}
			if (len >= fstat.max_len)
				fstat.max_len = len;
			fstat.chars++;
			len++;
		}
	}
	close(fd);
	return (fstat);
}

t_dict		get_content_dict(char *filename, t_fstat fstat)
{
	t_dict di;
	int		fd;
	int		i;
	int		j;
	int		lb;
	char	buf[BUF_SIZE];

	fd = open(filename, O_RDONLY);
	if (fd > 0)
	{
		di = (t_dict){0,0,0,0,0,0,{0,0,0}};
		di.nums = malloc(sizeof(char*) * fstat.lines);
		di.names = malloc(sizeof(char*) * fstat.lines);
		while (read(fd, buf, fstat.max_len))
		{
			lb = 0;
			while (lb < fstat.lines)
			{
				if (check_format_dict(buf))
				{
					i = 0;
					di.nums[di.nu_ln] = malloc(sizeof(char) * fstat.max_len);
					get_num(&i, &di.nums[di.nu_ln], buf);
					skip_wsp(&i, buf);
					if (buf[i] == ':')
						i++;
					skip_wsp(&i, buf);
					di.names[di.na_ln] = malloc(sizeof(char) * fstat.max_len);
					get_name(&i, &di.names[di.na_ln], buf);
					printf("CONTENT : %s | %s |\n|||\n%s\n|||\n",di.nums[di.nu_ln],di.names[di.na_ln],buf);
				}
				if (buf[lb])
				lb++;
			}
		}
	}
	return (di);
}

t_dict		get_dict(char *filename)
{
	t_fstat	fstat;
	t_dict	dict;
	
	fstat = get_fstat_dict(filename);
	printf(" FSTAT : ln %ld | ch %ld | max_len %ld\n",fstat.lines,fstat.chars,fstat.max_len);
	dict.fstat = fstat;
	dict = get_content_dict(filename, fstat);
	return (dict);
}
