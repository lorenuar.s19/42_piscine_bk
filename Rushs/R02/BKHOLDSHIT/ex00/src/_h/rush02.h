/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush02.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/16 14:00:27 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/16 23:18:42 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RUSH02_H
# define RUSH02_H
# define BUF_SIZE 64

# include <unistd.h>
# include <fcntl.h>
# include <stdlib.h>
# include <stdio.h>

typedef struct	s_fstat
{
	long int	chars;
	long int	lines;
	long int 	max_len;
}				t_fstat;

typedef struct	s_dict
{
	char		**nums;
	char		**names;
	int			nu_ln;
	int			nu_str;
	int			na_ln;
	int			na_str;
	t_fstat		fstat;
}				t_dict;

void			pstr(char *str);

void			print_name(t_dict dict, int len, char c, char d);
void			convert(t_dict dict, char *str);

t_dict			get_dict(char *filename);
t_fstat			get_fstat_dict(char *filename);
t_dict			get_content_dict(char *filename, t_fstat fstat);

int				check_format_dict(char *raw);
void			get_num(int *i, char **num, char *str);
void			get_name(int *i, char **name, char *str);
void			skip_wsp(int *i, char *str);

char			*units(t_dict dict, char c);
char			*tens(t_dict dict, char c);
char			*special(t_dict dict, char c);
char			*big_numbers(t_dict dict, char c, int len);
char 			*bigbig_numbers(t_dict dict, char c, int len);

#endif
