/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_dict.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/16 21:43:27 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/17 11:12:34 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "_h/rush02.h"

int			check_format_dict(char *raw)
{
	int	i;

	i = 0;
	while ((raw[i] >= '0' && raw[i] <= '9'))
	{
		if (!((raw[i] >= '0' && raw[i] <= '9')))
			return (0);
		i++;
	}
	while ((raw[i] >= '\t' && raw[i] <= '\r') || raw[i] == ' ')
	{
		if(!((raw[i] >= '\t' && raw[i] <= '\r') || raw[i] == ' '))
			return (0);
		i++;
	}
	if (raw[i] == ':')
		i++;
	else
		return (0);
	while ((raw[i] >= '\t' && raw[i] <= '\r') || raw[i] == ' ')
	{
		if(!((raw[i] >= '\t' && raw[i] <= '\r') || raw[i] == ' '))
			return (0);
		i++;
	}
	while (raw[i] >= ' ' && raw[i] <= '~')
	{
		if (!(raw[i] >= ' ' && raw[i] <= '~'))
			return (0);
		i++;
	}
	if (raw[i] == '\n')
		return (1);
	return (0);
}

void		get_num(int *i, char **num, char *str)
{
	while (str[*i] >= '0' && str[*i] <= '9')
	{
		printf("NU SEGV | %d\n",*i);
		*num[*i] =  str[*i];
		printf("NU SEGV | %d\n",*i);
		*i = *i + 1;
	}
}

void		get_name(int *i, char **name, char *str)
{
	while (str[*i] >= '0' && str[*i] <= '9')
	{
		printf("NA SEGV | %d\n",*i);
		*name[*i] = str[*i];
		printf("NA SEGV | %d\n",*i);
		*i = *i + 1;
	}
}

void		skip_wsp(int *i, char *str)
{
	while ( (str[*i] >= '\t' && str[*i] <= '\r') || str[*i] == ' ')
		*i = *i + 1;
}
