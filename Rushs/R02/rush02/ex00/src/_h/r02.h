/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   r02.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/17 14:15:42 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/17 23:08:17 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef R02_H
# define R02_H
# define BUF_SIZE 1024
# include <unistd.h>
# include <fcntl.h>
# include <stdlib.h>
# include <stdio.h>

int		str_len(char *str);
int		str_num_len(char *str);
void	pstr(char *str);
void	pchar(char c);
char	*str_cpy(char *dest, char *src);
int		str_num_cmp(char *s1, char *s2, int num);
char	*str_num_cpy(char *dest, char *src, int num);

int		check_line(char *raw);
int		skip_sep(int *i, char *line);
void	skip_wspaces(int *i, char *raw);
char	*search_line(char **name, char *line, char *input);
char	*translator(char *input, const char *filename);

void	small_numbers(int len, char *filename, char *input);
void	big_numbers(int len, char *filename, char *input);
int		converter(char *filename, char *input);
char	*str_replace(char *dest);
char	*str_fill_0(char *dest);

#endif
