/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   translator.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/17 18:12:07 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/17 23:14:31 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "_h/r02.h"

void	skip_wspaces(int *i, char *raw)
{
	while ((raw[*i] >= '\t' && raw[*i] <= '\r') || raw[*i] == ' ')
	{
		if (!((raw[*i] >= '\t' && raw[*i] <= '\r') || raw[*i] == ' '))
			return ;
		*i = *i + 1;
	}
}

int		check_line(char *raw)
{
	int	i;

	i = 0;
	while ((raw[i] >= '0' && raw[i] <= '9'))
	{
		if (!((raw[i] >= '0' && raw[i] <= '9')))
			return (0);
		i++;
	}
	skip_wspaces(&i, raw);
	if (raw[i] == ':')
		i++;
	else
		return (0);
	skip_wspaces(&i, raw);
	while (raw[i] >= ' ' && raw[i] <= '~')
	{
		if (!(raw[i] >= ' ' && raw[i] <= '~'))
			return (0);
		i++;
	}
	return (1);
}

int		skip_sep(int *i, char *line)
{
	while (line[*i] >= '0' && line[*i] <= '9')
		*i = *i + 1;
	while ((line[*i] >= '\t' && line[*i] <= '\r') || line[*i] == ' ')
		*i = *i + 1;
	while (line[*i] == ':')
		*i = *i + 1;
	while ((line[*i] >= '\t' && line[*i] <= '\r') || line[*i] == ' ')
		*i = *i + 1;
	return (1);
}

char	*search_line(char **name, char *line, char *input)
{
	int		i;
	int		j;
	char	*num;

	num = malloc(sizeof(char) * str_len(line) + 1);
	str_num_cpy(num, line, str_num_len(line));
	if (!str_num_cmp(num, input, str_num_len(line)))
	{
		free(num);
		i = 0;
		if (!skip_sep(&i, line))
		{
			*name[0] = 0;
			return (*name);
		}
		j = 0;
		str_cpy(*name, &line[i]);
		return (*name);
	}
	*name[0] = 0;
	return (*name);
}

char	*translator(char *input, const char *filename)
{
	int		fd;
	char	buffer[1024];
	char	*line;
	char	*output;
	int		i;
	int		j;

	fd = open(filename, O_RDONLY);
	line = 0;
	line = malloc(sizeof(char) * BUF_SIZE);
	if (fd > 0)
	{
		while (read(fd, buffer, BUF_SIZE) > 0)
		{
			i = 0;
			while (i < BUF_SIZE)
			{
				j = 0;
				while (buffer[i] != '\n' && j < BUF_SIZE)
					line[j++] = buffer[i++];
				line[j] = '\0';
				if (check_line(line))
				{
					output = malloc(sizeof(char) * j);
					search_line(&output, line, input);
					if (output[0] != 0)
					{
						return (output);
					}
				}
				i++;
			}
		}
		close(fd);
	}
	return (line);
}
