/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   converter.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/17 13:59:53 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/17 23:15:04 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "_h/r02.h"

char	*str_replace(char *dest)
{
	int i;

	dest[0] = '1';
	i = 1;
	while (dest[i + 1])
	{
		dest[i] = '0';
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

char	*str_fill_0(char *dest)
{
	int i;

	i = 1;
	while (dest[i])
	{
		dest[i] = '0';
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

void	small_numbers(int len, char *filename, char *input)
{
	char	n[1];
	char	*free;

	if (len == 1)
	{
		free = translator(input, filename);
		pstr(free);
		/*free(free);*/
	}
	else if (len == 2)
	{
		free = translator(input, filename);
		pstr(free);
		/*free(free);*/
	}
	else if (len == 3)
	{
		small_numbers(1, filename, str_num_cpy(n, input, 1));
		free = translator(input, filename);
		pstr(free);
		/*free(free);*/
		small_numbers(2, filename, str_num_cpy(n, input, 2));
	}
}

void	big_numbers(int len, char *filename, char *input)
{
	(void)len;
	(void)filename;
	(void)input;
	return ;
}

int		converter(char *filename, char *input)
{
	int		len;

	len = str_len(input);
	if (len <= 3)
	{
		small_numbers(len, filename, input);
	}
	else if (len > 3)
	{
		big_numbers(len, filename, input);
	}
	return (0);
}
