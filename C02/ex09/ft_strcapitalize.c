/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/04 13:24:48 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/11 13:42:10 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_alpha(char c)
{
	return ((c >= 'A' && c <= 'Z') ||
			(c >= 'a' && c <= 'z') ? 1 : 0);
}

char	*ft_strcapitalize(char *str)
{
	char	*s;
	int		word;

	s = str;
	while (*str)
	{
		if (*str >= 'A' && *str <= 'Z')
			*str += ('a' - 'A');
		str++;
	}
	str = s;
	word = 0;
	while (*str)
	{
		if (!is_alpha(*str))
			word = 0;
		if ((is_alpha(*str) || (*str >= '0' && *str <= '9')) && !word)
		{
			if (is_alpha(*str))
				*str -= ('a' - 'A');
			word++;
		}
		str++;
	}
	return (s);
}
