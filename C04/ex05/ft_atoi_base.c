/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 09:31:27 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/13 14:11:25 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		num_less_more_wspace(char c)
{
	if ((c >= '0' && c <= '9') || (c >= '\t' && c <= '\r') ||
		c == ' ' || c == '-' || c == '+')
		return (1);
	return (0);
}

int		ft_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len])
		len++;
	return (len);
}

int		check_occur(char c, char *str)
{
	int	i;
	int	occur;

	i = 0;
	occur = 0;
	while (str[i])
	{
		if (c == str[i])
			occur++;
		if (occur > 1)
			return (1);
		i++;
	}
	return (0);
}

int		check_base(char *base)
{
	int		len;
	int		i;

	len = 0;
	i = 0;
	while (base[i])
		i++;
	len = i;
	if (len == 1)
		return (0);
	i = 0;
	while (base[i])
	{
		if (base[i] == '-' || base[i] == '+' ||
			(base[i] >= '\r' && base[i] <= '\t') || base[i] == ' ')
			return (0);
		if (check_occur(base[i], base))
			return (0);
		i++;
	}
	return (len);
}

int		ft_atoi_base(char *str, char *base)
{
	int		nbr;
	int		neg;
	int		i_base;

	if (check_base(base))
	{
		i_base = ft_strlen(base);
		nbr = 0;
		neg = 0;
		while (*str && num_less_more_wspace(*str))
		{
			if (*str == '-')
				neg++;
			else if (*str >= '0' && *str <= '9')
				nbr = (nbr * i_base) + (*str - '0');
			str++;
		}
		if (neg % 2)
			nbr *= -1;
		return (nbr);
	}
	return (0);
}
