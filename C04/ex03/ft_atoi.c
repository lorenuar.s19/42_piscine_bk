/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 10:27:30 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/15 10:17:23 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		num_less_more_wspace(char c)
{
	if ((c >= '0' && c <= '9') || (c >= '\t' && c <= '\r') ||
		c == ' ' || c == '-' || c == '+')
		return (1);
	return (0);
}

int		ft_atoi(char *str)
{
	int		nbr;
	int		neg;

	nbr = 0;
	neg = 0;
	while (*str && num_less_more_wspace(*str))
	{
		if (*str == '-')
			neg++;
		if ((*str >= '\t' && *str <= '\r') || *str == ' ')
			neg = 0;
		else if (*str >= '0' && *str <= '9')
			nbr = (nbr * 10) + (*str - '0');
		str++;
	}
	if (neg % 2)
		nbr *= -1;
	return (nbr);
}
