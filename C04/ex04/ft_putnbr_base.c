/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 11:56:40 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/15 10:17:58 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	pc(char c)
{
	write(1, &c, 1);
}

int		check_occur(char c, char *str)
{
	int	i;
	int	occur;

	i = 0;
	occur = 0;
	while (str[i])
	{
		if (c == str[i])
			occur++;
		if (occur > 1)
			return (1);
		i++;
	}
	return (0);
}

int		check_base(char *base)
{
	int		len;
	int		i;

	len = 0;
	i = 0;
	while (base[i])
		i++;
	len = i;
	if (len == 1)
		return (0);
	i = 0;
	while (base[i])
	{
		if (base[i] == '-' || base[i] == '+')
			return (0);
		if (check_occur(base[i], base))
			return (0);
		i++;
	}
	return (len);
}

void	sub_putnbr_base(int nbr, int len_base, char *base)
{
	if (nbr < 0)
	{
		pc('-');
		sub_putnbr_base(-nbr, len_base, base);
	}
	else if (nbr >= len_base)
	{
		sub_putnbr_base(nbr / len_base, len_base, base);
		sub_putnbr_base(nbr % len_base, len_base, base);
	}
	else
	{
		pc(base[nbr % len_base]);
	}
}

void	ft_putnbr_base(int nbr, char *base)
{
	int		len_base;

	if (nbr > -2147483648 && nbr < 2147483647)
	{
		len_base = check_base(base);
		if (len_base)
			sub_putnbr_base(nbr, len_base, base);
	}
	return ;
}
